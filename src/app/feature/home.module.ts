import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from '../login/login.component';

import { CustomErrorStateMatcher } from '../utilities/CustomErrorStateMatcher';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material/core';

import { ReactiveFormsModule } from '@angular/forms';

import { HomeService } from './services/home.service';
import { AlertDialogComponent } from '../alertdialog/alertdialog.component';

// Módulo Home, éste módulo contendrá aquellos componentes de inicio de la aplicación.
// Tales como componentes de Login, Signup, Reset Password, etc.
@NgModule({ 
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule
    ],
    declarations: [
        LoginComponent,
        AlertDialogComponent
    ],
    exports: [
        LoginComponent,
        AlertDialogComponent
    ],
    entryComponents: [
        AlertDialogComponent
    ],
    providers: [
        HomeService,
        { provide: CustomErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
    ]
})
export class HomeModule {

}
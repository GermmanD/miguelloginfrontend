import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { DashboardComponent } from '../dashboard/dashboard.component';

import { DashboardService } from './services/dashboard.service';

// Éste módulo "Dashboard" contendrá aquellos componentes que tienen algún de relación con la entidad del usuario o sus relaciones.
// Tales como componentes de perfil, de listas de algún tipo de relación, etc.
@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [
        DashboardComponent
    ],
    exports: [
        DashboardComponent
    ],
    providers: [
        DashboardService
    ]
})
export class DashboardModule {

}
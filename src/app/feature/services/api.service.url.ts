// Configuración de URL de la Web API.

export const URL : string = "http://localhost:52445";

export function GetFullPath(uri: string) : string {
    return URL + uri;
}
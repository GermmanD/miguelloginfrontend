import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as UrlApi from './api.service.url';
import { Observable } from 'rxjs/Observable';

import { LoginModel } from '../../models/LoginModel';
import { HttpHeaders } from '@angular/common/http';

// Servicio del módulo Home.
// Éste servicio contendrá acciones para Login, Signup, Forgot Password, etc.
@Injectable()
export class HomeService {
    constructor(private http: HttpClient) {

    }

    // Se devuelve un observable "frío" con la petición para hacer login a un usuario.
    // Éste observable es suscrito en el componente de Login.
    OnLoginPerformed(loginData: LoginModel) : Observable<Object> {
        return this.http.post(UrlApi.GetFullPath("/api/OAuth/Login"), loginData, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
    }
}
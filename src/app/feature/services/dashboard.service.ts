import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

import * as UrlApi from './api.service.url';

// Servicio del módulo Dashboard, éste servicio contendrá acciones para obtener perfil, listado de usuarios, etc.
@Injectable()
export class DashboardService {
    private httpOptions;

    constructor(private httpClient: HttpClient) {
        // Una vez llegado a este servicio, es porque hubo un logeo previo,
        // por lo que el localStorage contiene el id del usuario logeado, junto con su token de accesso y de refresco.
        // Se agrega el token de accesso al header de autorización para cuando se haga cada petición HTTP,
        // Éste se envíe y se pueda tener accesso a los controladores protegidos.
        // Ya que sin él, el backend devolverá estado 401 (No autorizado).
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("access_token")
            })
        };
    }

    // Acción para obtener un usuario dado su id.
    // Este observable debe ser suscrito en el componente del cual se llama.
    GetUserById(id: string) : Observable<Object> {
        return this.httpClient.get(UrlApi.GetFullPath('/api/Users/' + id), this.httpOptions);
    }
}
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

// Éste componente es un componente "Util",
// Es decir, se usará para cuando se desee desplegar una alerta desde cualquier componente y/o módulo.
@Component({
    selector: 'app-alertdialog',
    templateUrl: './alertdialog.component.html',
    styleUrls: ['./alertdialog.component.css']
})
export class AlertDialogComponent {
    // Se le inyecta MAT_DIALOG_DATA, ya que como es un componente "Util" tiene que ser general para todos los 
    // componentes y módulos, de manera de simplificar el proceso de mostrar alertas en otras partes de la aplicación.
    // Ésta inyección debe contener como cuerpo "title" y "message", el cuál será el contenido que se querrá mostrar en la alerta.
    constructor(@Inject(MAT_DIALOG_DATA) private data: string) {
    }

    ngOnInit() {
    }
}
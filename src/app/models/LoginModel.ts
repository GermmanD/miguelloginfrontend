// Modelo que se usará para enviar y validar credenciales de usuarios al backend.
export class LoginModel {
    public Email : string;
    public Password: string;

    constructor(email: string, password: string) {
        this.Email = email;
        this.Password = password;
    }
}
// Modelo que se usará para almacenar información de las entidades de los usuarios devueltos por el backend.
export class UserModel {
    public UserId: string;
    public Email: string;
    public EmailConfirmed: boolean;
    public PhoneNumber: string;
    public PhoneNumberConfirmed: boolean;
    public Username: string;
}
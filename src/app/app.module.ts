import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HomeModule } from './feature/home.module';
import { DashboardModule } from './feature/dashboard.module';
import { RouterModule, Router } from '@angular/router';

// Se importan las rutas con los componentes asociados.
import * as routes from './routing/app.routes.js';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // Se importa el módulo de rutas. Ya que la aplicación trabajará con el uso de Routing.
    RouterModule.forRoot(
      routes.appRoutes, {
        // Se habilita para llevar un "trackeo" de la ruta en el log, en producción tiene que cambiarse a false.
        enableTracing: true
      }
    ),
    BrowserModule,
    // Se importa el módulo "Core" y los "Features".
    // En el módulo Core irán los módulos principales para que funcione la aplicación.
    CoreModule,
    HomeModule,
    DashboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

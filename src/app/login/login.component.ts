import { Component, AfterViewInit } from '@angular/core';
import { ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomErrorStateMatcher } from '../utilities/CustomErrorStateMatcher';

import { HomeService } from '../feature/services/home.service';
import { LoginModel } from '../models/LoginModel';

import { Router } from '@angular/router';

import { MatDialog } from '@angular/material';

import { AlertDialogComponent } from '../alertdialog/alertdialog.component';

@Component({ 
    selector: 'app-login',
    styleUrls: ['./login.component.css'],
    templateUrl: './login.component.html'
})
export class LoginComponent implements AfterViewInit {
    private _loginForm : FormGroup;
    private _emailFormControl : FormControl;
    private _passwordFormControl : FormControl;
    private _matcher : CustomErrorStateMatcher;
    private _waitingForRequest: boolean;

    constructor(private elementRef: ElementRef, 
        private homeService: HomeService, 
        private router: Router,
        private dialog: MatDialog) {
        
        // Se inicializan los campos para los inputs con sus respectivas validaciones.
        this._emailFormControl = new FormControl('', [
            Validators.required,
            Validators.email
        ]);

        this._passwordFormControl = new FormControl('', [
            Validators.required
        ]);

        // El "matcher" es el objeto que ejecuta las validaciones, éste matcher debe ser enlazado desde el códig HTML
        // a los inputs que se quiere hacer las validaciones.
        this._matcher = new CustomErrorStateMatcher();

        // Se crea un FormGroup para almacenar cada FormControl.
        this._loginForm = new FormGroup({
            emailFormControl : this._emailFormControl,
            passwordFormControl : this._passwordFormControl
        });

        // Ésta variable se usa para cuando se haga la petición HTTP al backend
        // Se inhabilite el botón y se inicie una animación de cargando debajo de la forma.
        this._waitingForRequest = false;
    }

    ngOnInit() {
    }

    // Como en ngOnInit no se tiene accesso al DOM por la jerarquía de ejecución
    // se debe implementar ngAfterViewInit. Acá se cambia el color de fondo del body al iniciarse este componente (LoginComponent).
    ngAfterViewInit() {
        this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#d6d6d6';
    }

    // Acción que realiza la petición al backend para validar credenciales.
    private OnLoginUser() : void {
        let loginData : LoginModel = new LoginModel(this._emailFormControl.value, this._passwordFormControl.value);
        this._waitingForRequest = true;
        this.homeService.OnLoginPerformed(loginData).subscribe(response => {
            // Si todo salió bien, se guarda la respuesta en el LocalStorage y se redirecciona al Dashboard.
            this._waitingForRequest = false;
            
            localStorage.setItem("access_token", response["access_token"]);
            localStorage.setItem("refresh_token", response["refresh_token"]);
            localStorage.setItem("user_id", response["user_id"]);

            this.router.navigate(['/Dashboard']);
        }, failure => {
            // Sino, se muestra un mensaje de error.
            this._waitingForRequest = false;

            switch(failure.status) {
            case 400:
                this.dialog.open(AlertDialogComponent, {
                    data: {
                        title: "Error!",
                        message: "Email or Password incorrect."
                    }
                });
                break;
            case 500:
                this.dialog.open(AlertDialogComponent, {
                    data: {
                        title: "Woops!",
                        message: "Server error! Don't worry, we're working on it. ;)"
                    }
                });
                break;
            }
        });
    }
}
import { Routes } from '@angular/router';

import { LoginComponent } from '../login/login.component';
import { DashboardComponent } from '../dashboard/dashboard.component';

// Variable que contiene las rutas a los componentes que se renderizarán en el app.component.
export const appRoutes : Routes = [
    { path: 'Login', component: LoginComponent }, 
    { path: '', redirectTo: '/Login', pathMatch: 'full' },
    { path: 'Dashboard', component: DashboardComponent }
];
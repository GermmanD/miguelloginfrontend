import { Component } from '@angular/core';

import { DashboardService } from '../feature/services/dashboard.service';

import { UserModel } from '../models/UserModel';

import { Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
    private userModel : UserModel;

    constructor(private dashboardService: DashboardService, private router: Router) {
        // Si el localStorage no tiene los 3 elementos básicos, los cuales son: 
        // - Token de acceso.
        // - Token de refresco.
        // - Id de usuario.
        // Se redirecciona al Login.
        if(localStorage.length < 3) {
            router.navigate(['/Login']);
        }

        this.userModel = new UserModel();
    }

    ngOnInit() {
        // Al iniciarse el componente, se hace una petición HTTP al backend
        // para obtener toda la información del usuario que se acaba de logear.
        this.dashboardService.GetUserById(localStorage.getItem('user_id'))
            .subscribe(response => {
                // Si todo estuvo bien, se llena el modelo userModel y se muestra el correo por pantalla a través del HTML.
                this.userModel.Email                = response['email'];
                this.userModel.UserId               = response['userId'];
                this.userModel.PhoneNumber          = response['phoneNumber'];
                this.userModel.EmailConfirmed       = response['emailConfirmed'];
                this.userModel.PhoneNumberConfirmed = response['phoneNumberConfirmed'];
                this.userModel.Username             = response['username'];
            });
    }
}
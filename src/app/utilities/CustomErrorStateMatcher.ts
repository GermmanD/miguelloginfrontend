import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';

// Ésta es la clase que se usará para validar los Inputs de Angular Material.
export class CustomErrorStateMatcher implements ErrorStateMatcher {
    // Se valida si el campo está vacío o tiene un formato inválido.
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null) : boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
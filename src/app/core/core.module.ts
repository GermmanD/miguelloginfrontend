import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

// Módulo principal de la aplicación, en éste módulo se incluirán los módulos que son esenciales para que la aplicación inicie.
@NgModule({
    imports: [
        // Éste módulo se importa ya que la API de animaciones web no es soportada por todos los navegadores,
        // con éste módulo de agrega soporte a todos estos navegadores.
        BrowserAnimationsModule,
        // Se importa el módulo para realizar peticiones Http a través de HttpClient.
        HttpClientModule
    ]
})
export class CoreModule {
    
}